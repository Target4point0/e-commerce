/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.CustomerDirectory;
import Business.SupplierDirectory;
import Business.Users.Admin;
import java.awt.event.KeyEvent;
import Business.Users.Customer;
import Business.Users.Supplier;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
/**
 *
 * @author harshalneelkamal
 */
public class AdminCreateScreen extends javax.swing.JPanel {

    /**
     * Creates new form AdminScreen
     */
    private JPanel panelRight;
    private Admin admin;
    public AdminCreateScreen(JPanel panelRight, Admin admin) {
        initComponents();
        this.panelRight = panelRight;
        this.admin = admin;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreate = new javax.swing.JButton();
        txtUser = new javax.swing.JTextField();
        txtPword = new javax.swing.JTextField();
        txtRePword = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        radioCustomer = new javax.swing.JRadioButton();
        radioSupplier = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        txtRePword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRePwordActionPerformed(evt);
            }
        });

        jLabel1.setText("username:");

        jLabel2.setText("password:");

        jLabel3.setText("re-enter password :");

        radioCustomer.setText("Customer");
        radioCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCustomerActionPerformed(evt);
            }
        });

        radioSupplier.setText("Supplier");
        radioSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioSupplierActionPerformed(evt);
            }
        });

        btnBack.setText("< BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtPword, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5))
                            .addComponent(txtRePword, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(radioSupplier)
                                    .addComponent(radioCustomer)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(btnBack)))
                .addContainerGap(534, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRePword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioCustomer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSupplier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCreate)
                .addContainerGap(47, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    
    public static boolean validateUsername(String username) {                 
                 
       boolean status_username=false;    
       String USERNAME_PATTERN = "^[^-_][_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
               //"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$";
       Pattern pattern = Pattern.compile(USERNAME_PATTERN);
       Matcher matcher=pattern.matcher(username);
       if(matcher.matches())
       {
           status_username=true;
       }
       else{
           status_username=false;
       }
           return status_username;
    }
     
    private static boolean validatePassword(String password) {                 
                 
       boolean status_password=false;    
     //  String PASSWORD_PATTERN = ;
       Pattern pattern = Pattern.compile("^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[+_$]).{6,20})$");
       Matcher matcher=pattern.matcher(password);
       if(matcher.matches())
       {
           status_password=true;
       }
       else{
           status_password=false;
       }
           return status_password;
    }
 
    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        
        if(!txtUser.getText().isEmpty()&&!txtPword.getText().isEmpty()&&(radioCustomer.getSelectedObjects()!=null||radioSupplier.getSelectedObjects()!=null))
        {
            if(txtPword.getText().equals(txtRePword.getText()))
            {
                txtRePword.setBackground(Color.GREEN);
                
                    if(radioCustomer.getSelectedObjects()!=null)
                    {
                        String username =txtUser.getText();
                        String password = txtPword.getText();
//                        String rePassword = txtRePword.getText(); 
                        
                        boolean usernameStatus= false;
                        boolean passwordStatus= false;
      // while(status!=true){
                        usernameStatus = AdminCreateScreen.validateUsername(txtUser.getText());
                        passwordStatus = AdminCreateScreen.validatePassword(txtPword.getText());
        
                        if (passwordStatus){
                            jLabel5.setText("Password Valid");
                        }
                        else{
                            jLabel5.setText("Not a Valid Password");
                        }
                        if (usernameStatus){
                            jLabel4.setText("Username Valid");
                        }
                        else{
                            jLabel4.setText("Not a Valid Username");
                        }
                        
                        if(usernameStatus && passwordStatus){
                        Customer newCustomer = new Customer(txtPword.getText(),txtUser.getText());
                        newCustomer.setCreatedOn(new Date());
                        CustomerDirectory cusDir = admin.getCustDir();
                        cusDir.addCustomer(newCustomer);
                        JOptionPane.showMessageDialog(null,"New Customer created successfully");
                        txtUser.setText("");
                        txtPword.setText("");
                        txtRePword.setText("");
                        txtRePword.setBackground(Color.GREEN);
                        radioCustomer.setSelected(false);
                        }
                        else{
                            JOptionPane.showMessageDialog(null,"Please enter valid details");
                        }
                    }
                    else if (radioSupplier.getSelectedObjects()!=null)
                    {
                        String username =txtUser.getText();
                        String password = txtPword.getText();
//                        String rePassword = txtRePword.getText();
                        boolean usernameStatus= false;
                        boolean passwordStatus= false;
      // while(status!=true){
                        usernameStatus = AdminCreateScreen.validateUsername(txtUser.getText());
                        passwordStatus = AdminCreateScreen.validatePassword(txtPword.getText());
        
                        if (passwordStatus){
                            jLabel5.setText("Password Valid");
                        }
                        else{
                            jLabel5.setText("Not a Valid Password");
                        }
                        if (usernameStatus){
                            jLabel4.setText("Username Valid");
                        }
                        else{
                            jLabel4.setText("Not a Valid Username");
                        }
                        
                        if(usernameStatus && passwordStatus){
                        Supplier newSupplier = new Supplier(txtPword.getText(),txtUser.getText());
                        newSupplier.setCreatedOn(new Date());
                        SupplierDirectory supDir = admin.getSuppDir();
                        supDir.addSupplier(newSupplier);
                        JOptionPane.showMessageDialog(null, "New Supplier Created");
                        txtUser.setText("");
                        txtPword.setText("");
                        txtRePword.setText("");
                        txtRePword.setBackground(Color.RED);
                        radioSupplier.setSelected(false);
                        }
                        else{
                            JOptionPane.showMessageDialog(null,"Please enter valid details");
                        }
                    }   
                }
        }
 
    }//GEN-LAST:event_btnCreateActionPerformed

    private void radioCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCustomerActionPerformed
        if (radioCustomer.isSelected())
            radioSupplier.setSelected(false);        
    }//GEN-LAST:event_radioCustomerActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        CardLayout layout = (CardLayout)panelRight.getLayout();
        panelRight.remove(this);
        layout.previous(panelRight);
    }//GEN-LAST:event_btnBackActionPerformed

    private void radioSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioSupplierActionPerformed
        if(radioSupplier.isSelected())
            radioCustomer.setSelected(false);
    }//GEN-LAST:event_radioSupplierActionPerformed

    private void txtRePwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRePwordActionPerformed
        // TODO add your handling code here:
//       // checkForButtonVisibility();
        
//        String check=""+evt.getKeyChar();
//        String compare=txtRePword.getText();
//        if(!check.isEmpty()){
//            compare += check;
//        }
//        if(txtPword.getText().equals(compare)){
//        txtRePword.setBackground(Color.white);
//        btnCreate.setEnabled(true);
//        }
//        else{
//        txtRePword.setBackground(Color.red);
//        btnCreate.setEnabled(false);
//          }
        
        
    }//GEN-LAST:event_txtRePwordActionPerformed

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButton radioCustomer;
    private javax.swing.JRadioButton radioSupplier;
    private javax.swing.JTextField txtPword;
    private javax.swing.JTextField txtRePword;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables

    private void checkForButtonVisibility() {
       if(!txtUser.getText().isEmpty() && !txtPword.getText().isEmpty() && !txtRePword.getText().isEmpty()){
       btnCreate.setEnabled(true);
       }
       else
           btnCreate.setEnabled(false);
        
    }
}
